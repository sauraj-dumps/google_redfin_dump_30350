## redfin-user 13 TPB4.220624.005 8812298 release-keys
- Manufacturer: google
- Platform: lito
- Codename: redfin
- Brand: google
- Flavor: redfin-user
- Release Version: 13
- Id: TPB4.220624.005
- Incremental: 8812298
- Tags: release-keys
- CPU Abilist: arm64-v8a,armeabi-v7a,armeabi
- A/B Device: true
- Locale: en-US
- Screen Density: undefined
- Fingerprint: google/redfin/redfin:13/TPB4.220624.005/8812298:user/release-keys
- OTA version: 
- Branch: redfin-user-13-TPB4.220624.005-8812298-release-keys
- Repo: google_redfin_dump_30350


>Dumped by [Phoenix Firmware Dumper](https://github.com/DroidDumps/phoenix_firmware_dumper)
